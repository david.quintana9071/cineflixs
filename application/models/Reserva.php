<?php
/**
 *
 */
class Reserva extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function insertar($datos){
  return $this->db->insert("reserva",$datos);
  }
  // Función de consulta todos los empleados de la base de datos
    public function obtenerTodos(){
      $ayudas=$this->db->get("reserva");
      if ($ayudas->num_rows()>0) {
        return $ayudas;
      } else {
        return false; //cuando no hay datos
      }
    }
    //funcion para dar por atendido ayuda
    public function eliminarPorId($id){
      $this->db->where("id_res",$id);
      return $this->db->delete("reserva");

    }
}
