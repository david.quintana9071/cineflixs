<?php
/**
 *
 */
class Ayuda extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  //funcion para insertar datos
  public function insertar($datos){
      return $this->db->insert("ayuda",$datos);
    }
  // Función de consulta todos los empleados de la base de datos
    public function obtenerTodos(){
      $ayudas=$this->db->get("ayuda");
      if ($ayudas->num_rows()>0) {
        return $ayudas;
      } else {
        return false; //cuando no hay datos
      }
    }
    //funcion para dar por atendido ayuda
    public function eliminarPorId($id){
      $this->db->where("id_lbq",$id);
      return $this->db->delete("ayuda");

    }
}
