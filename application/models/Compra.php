<?php
/**
 *
 */
class Compra extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  public function insertar($datos){
  return $this->db->insert("compra_ticket",$datos);
  }
  // Función de consulta todos los empleados de la base de datos
    public function obtenerTodos(){
      $ayudas=$this->db->get("compra_ticket");
      if ($ayudas->num_rows()>0) {
        return $ayudas;
      } else {
        return false; //cuando no hay datos
      }
    }
    //funcion para dar por atendido ayuda
    public function eliminarPorId($id){
      $this->db->where("id_com",$id);
      return $this->db->delete("compra_ticket");

    }
}
