<?php
  class Pelicula extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    public function insertar($datos){
      return $this->db->insert("pelicula",$datos);
    }
    // Función de consulta todos los jugadores de la base de datos
    public function obtenerTodos(){
      $jugadores=$this->db->get("pelicula");
      if ($jugadores->num_rows()>0) {
        return $jugadores;
      } else {
        return false; //cuando no hay datos
      }
    }
    //función para eliminar un jugador se recibe el //
    public function eliminarPorId($id){
      $this->db->where("id",$id);
      return $this->db->delete("pelicula");
    }
    //Consultando el jugador por su id
    public function ObtenerPorId($id){
      $this->db->where("id",$id);
      $jugador=$this->db->get("pelicula");
      if ($jugador->num_rows()>0){
        return $jugador->row(); //porque solo hay uno
      } else {
        return false;
      }
    }
    //Proceso de actualización de jugadores
    public function actualizar($id,$datos){
      $this->db->where("id",$id);
      return $this->db->update("pelicula",$datos);
    }
  }//cierre de la clase (No borrar)
