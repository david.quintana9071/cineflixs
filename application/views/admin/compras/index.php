<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">
            <i class="bi bi-tag"></i>
            Gestión Preventas
        </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/Welcome'); ?>">Inicio</a></li>
            <li class="breadcrumb-item active">Tabla</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Gestión de Preventas
            </div>
            <div class="card-body">
                <?php if ($listadoCompras): ?>
                <table id="miTabla">
                    <thead>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <th>ID</th>
                            <?php endif; ?>
                            <th>Cédula</th>
                            <th>Primer Apellido</th>
                            <th>Segundo Apellido</th>
                            <th>Nombre</th>
                            <th>Función</th>
                            <th>Teléfono</th>
                            <th>Dirección</th>
                            <th>Asientos</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoCompras->result() as $empleadoTemporal): ?>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <td class="text-center"><?php echo $empleadoTemporal->id_com ; ?></td>
                            <?php endif; ?>
                            <td class="text-center"><?php echo $empleadoTemporal->cedula_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->primer_apellido_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->segundo_apellido_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->segundo_apellido_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->funcion_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->telefono_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->direccion_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->asientos_com; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->precio_com; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <td></td>
                            <?php endif; ?>
                            <td><strong>TOTAL VENTAS:</strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th class="text-center" style="vertical-align: middle;">
                                <?php
                                $totalCosto = 0;
                                foreach ($listadoCompras->result() as $empleadoTemporal) {
                                    $totalCosto += $empleadoTemporal->precio_com;
                                }
                                echo $totalCosto;
                                ?>
                            </th>
                        </tr>
                    </tfoot>
                </table>
                <?php else: ?>
                <h3 class="text-center"><b>NO EXISTE NINGUNA COMPRA, VERIFICA NUEVAMENTE</b></h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>
