<div class="row">
  <div class="col-md-12">
    <?php if ($peliculaEditar): ?>
        <form id="frm_actualizar_jugador" class="" enctype="multipart/form-data" action="<?php echo site_url('member/Peliculas/procesarActualizacion'); ?>" method="post">
          <center> <input type="hidden" name="id" value="<?php echo $peliculaEditar->id; ?> "> </center>
          <div class="row">
            <div class="col-md-4 text-md-end">
              <label for="" style="font-weight: bold; font-family: 'Arial', sans-serif;">NOMBRE:</label>
            </div>
          <div class="col-md-7">
              <input type="text" id="nombre" name="nombre" value="<?php echo $peliculaEditar->nombre; ?>" class="form-control" placeholder="Ingrese los dos nombres">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-md-end">
              <label for="" style="font-weight: bold; font-family: 'Arial', sans-serif;">FOTOGRAFÍA:</label>
            </div>
          <div class="col-md-7">
              <input type="file" id="imagen" name="imagen" value="<?php echo $peliculaEditar->imagen; ?>" accept="image/png, image/jpg">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-7">
              <button type="submit" name="button" class="btn btn-warning"><i class="glyphicon glyphicon-ok"></i> Actualizar</button>
              <a href="<?php echo site_url('member/Peliculas/index');?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancelar</a>
            </div>
          </div>
      </form>
    <?php else: ?>
      <div class="alert alert-danger">
        <b>No se encontró al jugador :s</b>
      </div>
    <?php endif; ?>
  </div>
</div>
