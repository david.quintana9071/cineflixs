<main>
  <div class="container-fluid px-4">
<legend class="text-center">
  <center><h1 class="mt-4">
      <i class="bi bi-film"></i>
      Gestión Preventas
  </h1>
  </center>
  <a href="<?php echo site_url('member/Peliculas/nuevo');?>" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i> Agregar Nuevo</a>

</legend>
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="<?php echo site_url('admin/home'); ?>">Inicio</a></li>
    <li class="breadcrumb-item active">Admin-Película</li>
</ol>
<hr>
<?php if ($listadoPeliculas): ?>
  <table id="tbl-jugadores" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">FOTOGRAFÍA</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPeliculas->result() as $jugadorTemporal): ?>
        <tr>
          <td class="text-center"><?php echo $jugadorTemporal->id; ?></td>
          <td class="text-center"><?php echo $jugadorTemporal->nombre; ?></td>
          <td class="text-center"><?php if ($jugadorTemporal->imagen!=""): ?> <a href="<?php echo base_url('uploads/peliculas').'/'.$jugadorTemporal->imagen; ?> "target="_blank">
              <img src="<?php echo base_url('uploads/peliculas').'/'.$jugadorTemporal->imagen; ?> "width="50px" height="50px" alt=""></a>
                  <?php else: ?>
                    N/A
                  <?php endif; ?></td>
          <td class="text-center">
            <a href="<?php echo site_url('member/Peliculas/actualizar'); ?>/<?php echo $jugadorTemporal->id; ?>" class="btn btn-warning"><i class="glyphicon glyphicon-edit"></i> Editar</a>
            <a href="<?php echo site_url('member/Peliculas/borrar'); ?>/<?php echo $jugadorTemporal->id; ?>" class="btn btn-danger" onclick="return confirm('¿Está seguro de eliminar?');"><i class="glyphicon glyphicon-trash"></i> Eliminar</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3 class="text-center"><b>NO EXISTEN JUGADORES</b></h3>
<?php endif; ?>
<script type="text/javascript"> $("#tbl-jugadores").DataTable(); </script>
</div>
</main>
