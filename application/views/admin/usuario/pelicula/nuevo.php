<main>
  <div class="container-fluid px-4">
    <center><h1 class="mt-4">
        <i class="bi bi-film"></i>
        Gestión Preventas
    </h1>
    </center>

    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="<?php echo site_url('admin/home'); ?>">Inicio</a></li>
        <li class="breadcrumb-item active">Admin-Película</li>
    </ol>
    <div class="card mb-4">
      <div class="card-header">
          <i class="fas fa-table me-1"></i>
          Gestión de Películas
      </div>
      <div class="card-body">
<form id="frm_nuevo_jugador" class="" enctype="multipart/form-data" action="<?php echo site_url('member/Peliculas/guardarPelicula'); ?>" method="post">
    <div class="row">
      <div class="col-md-4 text-md-end">
        <label for="" style="font-weight: bold; font-family: 'Arial', sans-serif;">NOMBRE:</label>
      </div>
    <div class="col-md-7">
        <input type="text" id="nombre" name="nombre" value="" class="form-control" required placeholder="Ingrese el nombre de la pelicula">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4 text-md-end">
        <label for=""style="font-weight: bold; font-family: 'Arial', sans-serif;">FOTOGRAFÍA:</label>
      </div>
    <div class="col-md-7">
        <input type="file" id="imagen" name="imagen" value="" required accept="image/png, image/jpg">
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-7">
        <button type="submit" name="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Guardar</button>
        <a href="<?php echo site_url('member/Peliculas/index');?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancelar</a>
      </div>
    </div>
</form>

</div>
</div>
</div>
</main>
