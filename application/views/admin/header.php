<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Admin-CineFlix</title>
        <link rel="icon" href="<?php echo base_url('assets/iconos/admin.gif'); ?>" type="image/gif">
        <link href="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/style.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>/plantilla/admin/css/styles.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.7.2/font/bootstrap-icons.min.css">
        <script src="https://use.fontawesome.com/releases/v6.3.0/js/all.js" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/tabla/datatables.css">
        

        <style>
          .pre-loader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #ffffff;
            z-index: 9999;
            display: flex;
            justify-content: center;
            align-items: center;
          }

          .pre-loader-box {
            text-align: center;
          }

          .loader-logo img {
            width: 100px;
            height: 100px;
          }

          .loader-progress {
            width: 200px;
            height: 10px;
            background-color: #f1f1f1;
            margin: 20px auto;
          }

          .bar {
            width: 0;
            height: 100%;
            background-color: #007bff;
          }

          .percent {
            margin-top: 10px;
            font-weight: bold;
          }

          .loading-text {
            margin-top: 20px;
          }
        </style>


    </head>
    <body class="sb-nav-fixed">
      <div class="pre-loader">
        <div class="pre-loader-box">
          <h1 class="heading gsap-reveal-hero"
              style="text-shadow: -1px -1px 0 white, 1px -1px 0 white, -1px 1px 0 white, 1px 1px 0 white; font-family: 'Bangers', cursive; letter-spacing: 8px;">
              <span style="color: #000000;">Cineflix</span>
              <img src="<?php echo base_url('assets/iconos/admin.gif') ?>" alt="Preloader" class="zoom-image">
          </h1>
          <div class='loader-progress' id="progress_div">
            <div class='bar' id='bar1'></div>
          </div>
          <div class='percent' id='percent1'>0%</div>
          <div class="loading-text">
            Un momento porfavor...
          </div>
        </div>
      </div>



        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="<?php echo site_url('admin/Home'); ?>">AdminCineflix <img src="<?php echo base_url('plantilla/images/cineflixlogo.png'); ?>" alt="Logo de CineFlix" width="50px" height="50px"></a>
            <!-- Sidebar Toggle-->
            <button class="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0" id="sidebarToggle" href="#!"><i class="fas fa-bars"></i></button>
            <!-- Navbar Search-->
            <form class="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
                <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search for..." aria-label="Search for..." aria-describedby="btnNavbarSearch" />
                    <button class="btn btn-primary" id="btnNavbarSearch" type="button"><i class="fas fa-search"></i></button>
                </div>
            </form>
            <!-- Navbar-->
            <ul class="navbar-nav ms-auto ms-md-0 me-3 me-lg-4">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#!">Settings</a></li>
                        <li><a class="dropdown-item" href="#!">Activity Log</a></li>
                        <li><hr class="dropdown-divider" /></li>
                        <li><a class="dropdown-item" href="<?php echo site_url('welcome/logout'); ?>">Salir</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <div class="sb-sidenav-menu-heading">Inicio</div>
                            <a class="nav-link" href="<?php echo site_url('admin/Home'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Panel
                            </a>
                            <div class="sb-sidenav-menu-heading" style="color: white;"><i class="bi bi-film"> Usuarios</i></div>
                            <a class="nav-link" href="<?php echo site_url('member/Peliculas/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Peliculas
                            </a>
                            <a class="nav-link" href="<?php echo site_url('admin/Compras/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Cartelera
                            </a>
                            <a class="nav-link" href="<?php echo site_url('admin/Compras/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Cliente
                            </a>
                            <a class="nav-link" href="<?php echo site_url('admin/Compras/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Tarjeta
                            </a>

                            <div class="sb-sidenav-menu-heading" style="color: white;"><i class="bi bi-person-fill"></i> Administración</div>

                            <a class="nav-link" href="<?php echo site_url('admin/Compras/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Compras
                            </a>
                            <a class="nav-link" href="<?php echo site_url('admin/Reservas/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Preventas
                            </a>


                            <div class="sb-sidenav-menu-heading" style="color: white;"><i class="bi bi-headset"></i> Atención-Cliente</div>
                            <a class="nav-link" href="charts.html">
                                <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                                Charts
                            </a>
                            <a class="nav-link" href="<?php echo site_url('admin/Ayudas/index'); ?>">
                                <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                                Gestión-Ayudas
                            </a>
                        </div>
                    </div>
                    <div class="sb-sidenav-footer">
                        <div class="small">CineFlix<img src="<?php echo base_url('assets/palomitas.gif') ?>" alt="Preloader" class="zoom-image" style="width: 50px; height: 50px;"></div>
                        Ticket Online
                    </div>
                </nav>
            </div>
<div id="layoutSidenav_content">
