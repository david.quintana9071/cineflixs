<main>
  <div class="container-fluid px-4">
    <h1 class="mt-4">
        <i class="bi bi-headset"></i>
        Gestión Ayuda  al cliente
    </h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="<?php echo site_url('admin/Welcome'); ?>">Inicio</a></li>
        <li class="breadcrumb-item active">Tabla</li>
    </ol>
    <div class="card mb-4">
      <div class="card-header">
          <i class="fas fa-table me-1"></i>
          Gestión de Ayuda al cliente
      </div>
      <div class="card-body">
        <table id="miTabla">
          <thead>
              <tr>
                <?php if ($mostrarIdColumn): ?>
                  <th class="text-center">ID</th>
                <?php endif; ?>
                <th>CORREO</th>
                <th>USUARIO</th>
                <th>MOTIVO</th>
                <th>ASUNTO</th>
                <th>DESCRIPCIÓN</th>
                <th>ACCIÓN</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($listadoAyudas->result() as $empleadoTemporal): ?>
                <tr>
                  <?php if ($mostrarIdColumn): ?>
                    <td class="text-center"><?php echo $empleadoTemporal->id_lbq; ?></td>
                  <?php endif; ?>
                  <td class="text-center"><?php echo $empleadoTemporal->correo_lbq; ?></td>
                  <td class="text-center"><?php echo $empleadoTemporal->usuario_lbq; ?></td>
                  <td class="text-center"><?php echo $empleadoTemporal->motivo_lbq; ?></td>
                  <td class="text-center"><?php echo $empleadoTemporal->asunto_lbq; ?></td>
                  <td class="text-center"><?php echo $empleadoTemporal->descripcion_lbq; ?></td>
                  <td>
                    <center>
                      <a href="<?php echo site_url('admin/Ayudas/borrar'); ?>/<?php echo $empleadoTemporal->id_lbq; ?>" class="btn btn-success" onclick="return confirm('¿Está seguro de eliminar?');">
                        <i class="bi bi-headset"></i> Atendido
                      </a>
                    </center>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <?php if ($mostrarIdColumn): ?>
                  <th class="text-center">ID</th>
                <?php endif; ?>
                <th>CORREO</th>
                <th>USUARIO</th>
                <th>MOTIVO</th>
                <th>ASUNTO</th>
                <th>DESCRIPCIÓN</th>
                <th>ACCIÓN</th>
              </tr>

            </tfoot>
        </table>
</div>
</div>
</main>
