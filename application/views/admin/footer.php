<footer class="py-4 bg-light mt-auto">
    <div class="container-fluid px-4">
        <div class="d-flex align-items-center justify-content-between small">
            <div class="text-muted">Copyright &copy; Your Website 2023</div>
            <div>
                <a href="#">Privacy Policy</a>
                &middot;
                <a href="#">Terms &amp; Conditions</a>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>/plantilla/admin/js/scripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>/plantilla/admin/assets/demo/chart-area-demo.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/admin/assets/demo/chart-bar-demo.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/simple-datatables@7.1.2/dist/umd/simple-datatables.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>/plantilla/admin/js/datatables-simple-demo.js"></script>-->
<script>
  var width = 1;
  var progressBar = document.getElementById('bar1');
  var percentText = document.getElementById('percent1');

  function animateLoader() {
    if (width >= 100) {
      clearInterval(progressBarInterval);
      setTimeout(function () {
        var preLoader = document.querySelector('.pre-loader');
        preLoader.style.display = 'none';
      }, 500);
    } else {
      width++;
      progressBar.style.width = width + '%';
      percentText.textContent = width + '%';
    }
  }

  var progressBarInterval = setInterval(animateLoader, 20);
</script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/tabla/datatables.js"></script>
  <script>
    $(document).ready(function() {
      $('#miTabla').DataTable();
    });
  </script>


</body>
</html>
