<main>
    <div class="container-fluid px-4">
        <h1 class="mt-4">
            <i class="bi bi-tag"></i>
            Gestión Preventas
        </h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/Home'); ?>">Inicio</a></li>
            <li class="breadcrumb-item active">Tabla</li>
        </ol>
        <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table me-1"></i>
                Gestión de Preventas
            </div>
            <div class="card-body">
                <?php if ($listadoReservas): ?>
                <table id="miTabla" class="table">
                    <thead>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <th>ID</th>
                            <?php endif; ?>
                            <th>NOMBRE</th>
                            <th>PELÍCULA</th>
                            <th>ACIENTO</th>
                            <th>CATEGORÍA</th>
                            <th>FECHA</th>
                            <th>VALOR Ticket</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($listadoReservas->result() as $empleadoTemporal): ?>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <td class="text-center"><?php echo $empleadoTemporal->id_res; ?></td>
                            <?php endif; ?>
                            <td class="text-center"><?php echo $empleadoTemporal->nombre_res; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->pelicula_res; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->aciento_res; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->descripcion_res; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->fecha_res; ?></td>
                            <td class="text-center"><?php echo $empleadoTemporal->costo_res; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <?php if ($mostrarIdColumn): ?>
                            <td></td>
                            <?php endif; ?>
                            <td><strong>TOTAL VENTA:</strong></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th class="text-center" style="vertical-align: middle;">
                                <?php
                                $totalCosto = 0;
                                foreach ($listadoReservas->result() as $empleadoTemporal) {
                                    $totalCosto += $empleadoTemporal->costo_res;
                                }
                                echo $totalCosto;
                                ?>
                            </th>
                        </tr>
                    </tfoot>
                </table>
                <?php else: ?>
                <h3 class="text-center"><b>No existen Reserva de películas</b></h3>
                <?php endif; ?>
            </div>
        </div>
    </div>
</main>
