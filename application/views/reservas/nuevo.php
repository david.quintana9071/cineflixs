<br><br><br><br>
<div class="unslate_co--section" id="contact-section">
    <div class="container">
        <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">¡Preventa de Ticket!</span></h2>
            <span class="gsap-reveal"><img src="<?php echo base_url('plantilla/images/divider.png') ?>" alt="divider"
                    width="76"></span>
        </div>


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <form method="post" action="<?php echo site_url('reservas/guardarReserva'); ?>"
                        class="form-outline-style-v1">
                        <div class="form-group row mb-0">

                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="name">Nombre usuario </label>
                                <input name="nombre_res" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Pelicula ¡Preventa</label>
                                <input name="pelicula_res" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Asiento ¡Preventa</label>
                                <input name="aciento_res" type="number" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Costo ¡Preventa</label>
                                <input name="costo_res" type="number" class="form-control" required>
                            </div>
                            <div class="col-lg-12 form-group gsap-reveal">
                                <label for="">Descripción ¡Preventa</label>
                                <textarea name="descripcion_res" id="" cols="30" rows="7"
                                    class="form-control"></textarea>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <input name="fecha_res" type="date" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row gsap-reveal">
                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                <input type="submit" class="btn btn-outline-pill btn-custom-light mr-3" value="Enviar">
                                <span class="submitting"></span>
                                <a href="<?php echo site_url('reservas/nuevo');?>"></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</div> <!-- END .unslate_co-site-inner -->
