<br><br><br><br>
<div class="unslate_co--section" id="contact-section">
    <div class="container">
        <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">¡Dime tú Ayuda!</span></h2>
            <span class="gsap-reveal"><img src="<?php echo base_url('plantilla/images/divider.png') ?>" alt="divider"
                    width="76"></span>
        </div>


        <div class="row justify-content-between">

            <div class="col-md-6">
                <form method="post" action="<?php echo site_url('ayudas/guardarAyuda'); ?>"
                    class="form-outline-style-v1">
                    <div class="form-group row mb-0">

                        <div class="col-lg-6 form-group gsap-reveal">
                            <label for="name">Correo electrónico</label>
                            <input name="correo_lbq" type="email" class="form-control" required>
                        </div>
                        <div class="col-lg-6 form-group gsap-reveal">
                            <label for="email">Usuario</label>
                            <input name="usuario_lbq" type="text" class="form-control" required>
                        </div>
                        <div class="col-lg-6 form-group gsap-reveal">
                            <label for="email">Motivo</label>
                            <input name="motivo_lbq" type="text" class="form-control" required>
                        </div>
                        <div class="col-lg-6 form-group gsap-reveal">
                            <label for="email">Asunto</label>
                            <input name="asunto_lbq" type="text" class="form-control" required>
                        </div>
                        <div class="col-lg-12 form-group gsap-reveal">
                            <label for="message">Dime la descripción de tu problema...</label>
                            <textarea name="descripcion_lbq" id="message" cols="30" rows="7"
                                class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row gsap-reveal">
                        <div class="col-md-12 d-flex align-items-center">
                            <input type="submit" class="btn btn-outline-pill btn-custom-light mr-3" value="Enviar">
                            <span class="submitting"></span>
                            <a href="<?php echo site_url('ayudas/nuevo');?>"></a>
                        </div>
                    </div>
                </form>
                <div id="form-message-warning" class="mt-4"></div>
                <div id="form-message-success">
                    Your message was sent, thank you!
                </div>
            </div>

            <div class="col-md-4">
              <h1 style="color: #FFFFFF;">Contacto Directo</h1>
                <div class="contact-info-v1">
                    <div class="gsap-reveal d-block">
                        <span class="d-block contact-info-label">Correo</span>
                        <a href="#" class="contact-info-val">david.quintana@gmail.com</a>
                    </div>
                    <div class="gsap-reveal d-block">
                        <span class="d-block contact-info-label">Celular</span>
                        <a href="#" class="contact-info-val">+593 995551281</a>
                    </div>
                    <div class="gsap-reveal d-block">
                        <span class="d-block contact-info-label">Dirección</span>
                        <address class="contact-info-val">Ecuador <br> Pichincha, Machachi</address>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div> <!-- END .unslate_co-site-inner -->
