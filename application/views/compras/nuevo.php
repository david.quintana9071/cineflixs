<br><br><br><br>
<div class="unslate_co--section" id="contact-section">
    <div class="container">
        <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">¡Compra de Ticket!</span></h2>
            <span class="gsap-reveal"><img src="<?php echo base_url('plantilla/images/divider.png') ?>" alt="divider"
                    width="76"></span>
        </div>


        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <form method="post" action="<?php echo site_url('compras/guardarCompra'); ?>"
                        class="form-outline-style-v1">
                        <div class="form-group row mb-0">

                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="name">Cedula </label>
                                <input name="cedula_com" type="number" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Primer Apellido</label>
                                <input name="primer_apellido_com" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Segundo Apellido</label>
                                <input name="segundo_apellido_com" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Nombre</label>
                                <input name="nombre_com" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Función</label>
                                <input name="funcion_com" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Teléfono</label>
                                <input name="telefono_com" type="number" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Dirección</label>
                                <input name="direccion_com" type="text" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Asientos</label>
                                <input name="asientos_com" type="number" class="form-control" required>
                            </div>
                            <div class="col-lg-6 form-group gsap-reveal">
                                <label for="">Precio</label>
                                <input name="precio_com" type="number" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row gsap-reveal">
                            <div class="col-md-12 d-flex align-items-center justify-content-center">
                                <input type="submit" class="btn btn-outline-pill btn-custom-light mr-3" value="Enviar">
                                <span class="submitting"></span>
                                <a href="<?php echo site_url('compras/nuevo');?>"></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</div> <!-- END .unslate_co-site-inner -->
