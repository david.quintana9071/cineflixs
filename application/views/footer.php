<br>
<br>
<br>
<footer class="unslate_co--footer unslate_co--section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7">
                <div class="footer-site-logo"><a href="#">CineFlix <img
                            src="<?php echo base_url('plantilla/images/cineflixlogo.png'); ?>" alt="Logo de CineFlix"
                            width="50px" height="50px"></a></div>
                <ul class="footer-site-social">
                    <li>
                        <a href="#">
                            <img class="logo" src="<?php echo base_url(); ?>assets/iconos/facebook.png" alt="Facebook">
                            Facebook
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img class="logo" src="<?php echo base_url(); ?>assets/iconos/twitter.png" alt="Twitter">
                            Twitter
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img class="logo" src="<?php echo base_url(); ?>assets/iconos/instagram.png"
                                alt="Instagram">
                            Instagram
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img class="logo" src="<?php echo base_url(); ?>assets/iconos/tiktok.png" alt="TikTok">
                            TikTok
                        </a>
                    </li>


                </ul>

                <p class="site-copyright">

                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    100% Original
                    <script>
                    document.write(new Date().getFullYear());
                    </script> Tu destino cinematográfico de primera.| ¡Disfruta la magia del cine con nosotros! <i
                        class="icon-heart" aria-hidden="true"></i> by <a href="" target="_blank">CineFlix</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

                </p>

            </div>
        </div>
    </div>
</footer>


</div>




<script src="<?php echo base_url(); ?>/plantilla/js/scripts-dist.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/js/main.js"></script>
<style>
.video-wrap {
    position: relative;
}

.video-overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    text-align: center;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 1;
}

/* Estilos adicionales para el contenido superpuesto */
/* Puedes personalizar los estilos según tus necesidades */
.video-overlay .heading {
    font-size: 36px;
    font-weight: bold;
}

.video-overlay .subheading {
    font-size: 24px;
}
</style>
<style>
.image-container {
    position: relative;
    display: inline-block;
}

.hover-effect {
    transition: opacity 0.3s;
    /* Transición suave */
}

.hover-effect:hover {
    opacity: 0.7;
    /* Opacidad reducida al mover el mouse sobre la imagen */
}

.hover-text {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: rgba(255, 255, 0, 0.7);
    /* Amarillo transparente */
    color: #000;
    /* Color del texto en negro */
    padding: 10px;
    font-family: "Arial", sans-serif;
    /* Tipo de letra llamativo */
    font-weight: bold;
    /* Texto en negrita */
    font-size: 20px;
    /* Tamaño de fuente más grande */
    opacity: 0;
    transition: opacity 0.3s;
}

.image-container:hover .hover-text {
    opacity: 1;
}
</style>
<style>
.logo {
    width: 20px;
    /* Ajusta el tamaño según sea necesario */
    height: 20px;
}
</style>
<script>
window.addEventListener("load", function() {
    const preloader = document.querySelector(".preloader");
    preloader.classList.add("hide");
});
</script>
<script>
$(document).ready(function() {
  $("#iniciarSesionBtn").click(function() {
    $("#popupContainer").fadeIn();
  });

  $(document).on("click", function(e) {
    if ($(e.target).closest("#iniciarSesionBtn, #popupContainer").length === 0) {
      $("#popupContainer").fadeOut();
    }
  });
});


</script>
<script>
  $(document).ready(function() {
    // Abrir la ventana emergente al hacer clic en el botón "Iniciar sesión"
    $('#loginButton').click(function() {
      $('#loginModal').modal('show');
    });
  });
</script>


</body>

</html>
