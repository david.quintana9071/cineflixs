<div class="video-wrap">
    <video autoplay loop muted class="custom-video" poster="">
        <source src="<?php echo base_url(); ?>/plantilla/video/video.mp4" type="video/mp4">
    </video>
    <style>
    .video-container {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: -1;
    }

    .custom-video {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    </style>
    <style>
    .circle {
        width: 100px;
        height: 100px;
        border-radius: 50%;
        background-color: #ccc;
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 0 auto;
        margin-bottom: 20px;
    }

    .circle img {
        width: 80px;
        height: 80px;
        object-fit: cover;
        border-radius: 50%;
    }
</style>
<br><br>
    <div class="video-overlay">
        <div class="container">
            <div class="row align-items-center">
              <div class="col-md-7 mx-auto text-center">
                  <style>
                      @import url('https://fonts.googleapis.com/css2?family=Bangers&display=swap');
                  </style>
                  <h1 class="heading gsap-reveal-hero" style="text-shadow: -1px -1px 0 white, 1px -1px 0 white, -1px 1px 0 white, 1px 1px 0 white; font-family: 'Bangers', cursive; letter-spacing: 8px; font-size: 28px;">
                      <span style="color: #FFA500;">Cine</span><span style="color: #00BFFF;">Flix</span>
                  </h1>

                  <div class="circle">
                      <img src="<?php echo base_url('assets/images/unnamed.jpg'); ?>" alt="Logo de CineFlix" width="80px" height="80px">
                  </div>

                  <h2 class="subheading gsap-reveal-hero" style="font-size: 20px;">¡Disfruta del fascinante mundo del cine, David Quintana!</h2>
              </div>
            </div>
        </div>
    </div>
</div>

<style>
.trailer-text {
    color: blue;
    /* Cambia el color a tu preferencia, por ejemplo, azul */
}
</style>
<div class="d-flex align-items-center justify-content-center mb-4 gsap-reveal gsap-reveal-filter">
    <h2 class="mr-auto heading-h2"><span class="gsap-reveal">Películas de estreno</span></h2>
</div>

<div class="owl-carousel">
    <div class="item">
        <a href="https://www.youtube.com/watch?v=ZMH8NWf9btw" target="_blank">
            <img src="<?php echo base_url('assets/images/joker.jpg'); ?>" class="hover-effect">
            <div class="hover-text">
                Ver Tráiler
            </div>
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=lQcocbUx4Lo" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen10.jpg'); ?>" class="hover-effect">
            <div class="hover-text">
                Ver Tráiler
            </div>
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=LTDdC71bN30" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen9.jpg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=kDGpcy91RQs" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen8.jpg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=O5BOxn8Go8U" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen7.jpg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=CLiniBPN_R4" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen6.jpg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=SvJwEiy2Wok" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen5.jpg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=v0d0id78XdE&t=2s" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen4.jpeg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=nFYA2kdHy0s&t=1s" target="_blank">
            <img src="<?php echo base_url('assets/images/imagen3.jpeg'); ?>" class="hover-effect">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=b3_1cyJRaQ8" target="_blank" class="hover-effect">
            <img src="<?php echo base_url('assets/images/imagen2.jpeg'); ?>">
        </a>
    </div>
    <div class="item">
        <a href="https://www.youtube.com/watch?v=oBmazlyP220" target="_blank" class="hover-effect">
            <img src="<?php echo base_url('assets/images/imagen1.jpg'); ?>">
        </a>
    </div>



    <!-- Agrega más divs con imágenes según tus necesidades -->
</div>


<script type="text/javascript">
$(document).ready(function() {
    // Inicializar el carrousel
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });
});
</script>
<div class="center-align">
    <a href="#portfolio-section" class="mouse-wrap smoothscroll">
        <span class="mouse">
            <span class="scroll"></span>
        </span>
        <span class="mouse-label">Clic aquí</span>
    </a>
</div>





<!-- END .cover-v1 -->


<div class="unslate_co--section" id="portfolio-section">
    <div class="container">


        <div class="relative">
            <div class="loader-portfolio-wrap">
                <div class="loader-portfolio"></div>
            </div>
        </div>
        <div id="portfolio-single-holder"></div>

        <div class="portfolio-wrapper">

            <div class="d-flex align-items-center mb-4 gsap-reveal gsap-reveal-filter">
                <h2 class="mr-auto heading-h2"><span class="gsap-reveal">Promociones</span></h2>

                <a href="#" class="text-white js-filter d-inline-block d-lg-none">Panel</a>

                <div class="filter-wrap">
                    <div class="filter ml-auto" id="filters">
                        <a href="#" class="active" data-filter="*">Todo</a>
                        <a href="#" data-filter=".web">Comida</a>
                        <a href="#" data-filter=".branding">Promociones</a>
                        <a href="#" data-filter=".illustration">Descuentos</a>
                        <a href="#" data-filter=".packaging">Especial</a>
                    </div>
                </div>
            </div>



            <div id="posts" class="row gutter-isotope-item">
                <div class="item web col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-link2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>
                <div class="item web col-sm-6 col-md-6 col-lg-4 isotope-mb-2 ">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada1.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-link2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada1.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item branding col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada2.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-link2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada2.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item illustration col-sm-6 col-md-6 col-lg-4 isotope-mb-2">

                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada3.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-photo2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada3.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>

                </div>

                <div class="item branding col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada4.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-photo2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada4.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item packaging col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada5.png"
                        lass="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-link2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada5.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item branding col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada6.png"
                        class="portfolio-item item-portrait isotope-item gsap-reveal-img " data-fancybox="gallery"
                        data-caption="Modern Building">
                        <div class="overlay">
                            <span class="wrap-icon icon-photo2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada6.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item branding col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada7.png"
                        class="portfolio-item isotope-item gsap-reveal-img" data-fancybox="gallery"
                        data-caption="Showreel 2019">
                        <div class="overlay">
                            <span class="wrap-icon icon-play_circle_filled"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada7.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>

                <div class="item illustration col-sm-6 col-md-6 col-lg-4 isotope-mb-2">
                    <a href="<?php echo base_url(); ?>/assets/promociones/Imagenpegada8.png" night
                        class="portfolio-item isotope-item gsap-reveal-img" data-fancybox="gallery"
                        data-caption="Render Packaging">
                        <div class="overlay">
                            <span class="wrap-icon icon-photo2"></span>
                            <div class="portfolio-item-content">

                            </div>
                        </div>
                        <img src="<?php echo base_url(); ?>/assets/promociones/Imagenpegada8.png"
                            class="lazyload  img-fluid" alt="Images" />
                    </a>
                </div>
            </div>


        </div>


    </div>
</div>

<div>
    <center>
        <h2 class="mr-auto heading-h2"><span class="gsap-reveal">También disponible en</span></h2>
    </center>

    <center><div class="unslate_co--section">
        <div class="container">
            <div class="owl-carousel logo-slider">
                <div class="logo-v1 gsap-reveal">
                    <img src="<?php echo base_url(); ?>/assets/images/googleplay.png" alt="Image" class="img-fluid">
                </div>
                <div class="logo-v1 gsap-reveal">
                    <img src="<?php echo base_url(); ?>/assets/images/apple.png" alt="Image" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
    </center>
</div>

<div class="unslate_co--section" id="about-section">
    <div class="container">

        <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Sobre nosotros</span></h2>
            <span class="gsap-reveal">
                <img src="<?php echo base_url(); ?>/plantilla/images/divider.png" alt="divider" width="76">
            </span>
        </div>


        <div class="row mt-5 justify-content-between">
            <div class="col-lg-7 mb-5 mb-lg-0">
                <figure class="dotted-bg gsap-reveal-img ">
                    <img src="<?php echo base_url(); ?>/assets/images/personal.png" alt="Image" class="img-fluid">
                </figure>
            </div>
            <div class="col-lg-4 pr-lg-5">
                <h3 class="mb-4 heading-h3"><span class="gsap-reveal">Nuestro Personal</span></h3>
                <p class="lead gsap-reveal" style="color: white;">Nuestro cine cuenta con un equipo de profesionales
                    apasionados por el cine, comprometidos en brindarte la mejor experiencia. Desde nuestros amigables
                    taquilleros hasta nuestros amantes del cine en el servicio de concesión, todos están aquí para
                    asegurarse de que disfrutes de cada momento. <a href="#" style="color: white;">Siempre dispuestos a
                        ayudarte,</a> nuestro personal hará que tu visita sea inolvidable.</p>
            </div>

        </div>
        <div class="row mt-5 justify-content-between">
            <div class="col-lg-4 pr-lg-5">
                <h3 class="mb-4 heading-h3"><span class="gsap-reveal">Nuestra sala de cine</span></h3>
                <p class="lead gsap-reveal" style="color: white;">Sumérgete en una experiencia cinematográfica sin igual
                    en nuestras salas de cine. Con tecnología de vanguardia, pantallas de alta definición y sonido
                    envolvente, te transportarás a otro mundo. Nuestras cómodas butacas y ambiente acogedor garantizan
                    que disfrutes cada película al máximo. Vive el cine como nunca antes</p>
            </div>
            <div class="col-lg-7 mb-5 mb-lg-0">
                <figure class="dotted-bg gsap-reveal-img">
                    <img src="<?php echo base_url(); ?>/assets/images/sala.png" alt="Image" class="img-fluid">
                </figure>
            </div>
        </div>

    </div>
</div>

<div class="unslate_co--section" id="services-section">
    <div class="container">

        <div class="section-heading-wrap text-center mb-5">
            <h2 class="heading-h2 text-center divider"><span class="gsap-reveal">Servicios</span></h2>
            <span class="gsap-reveal"><img src="<?php echo base_url(); ?>/plantilla/images/divider.png" alt="divider"
                    width="76"></span>
        </div>

        <div class="row gutter-v3">
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="0">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/cine.png" alt="Image" width="45">
                    </div>
                    <h3 style="color: white;">Amplias salas <br> de proyección</h3>
                    <p style="color: white;">Disfruta de nuestras amplias salas de proyección, diseñadas para brindarte
                        una experiencia cinematográfica inmersiva y confortable.</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="100">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/sistema-de-sonido.png" alt="Icon" width="45">
                    </div>
                    <h3 style="color: white;">Sonido <br> envolvente</h3>
                    <p style="color: white;">Sumérgete en el cine con nuestro sonido envolvente, que te transportará a
                        un mundo lleno de emociones y detalles auditivos.</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="200">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/experience.png" alt="Image" class="img-fluid"
                            width="45">
                    </div>
                    <h3 style="color: white;">Experiencia <br> Usuario</h3>
                    <p style="color: white;">Queremos ser parte de ti! por eso queremos que te lleves grandes
                        experiencias con nosotos. </p>
                </div>
            </div>

            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="0">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/pelicula-3d.png" alt="Image" width="45">
                    </div>
                    <h3 style="color: white;">Proyección en<br> 3D</h3>
                    <p style="color: white;">Vive la magia del cine en tercera dimensión con nuestras proyecciones en
                        3D, donde podrás disfrutar de efectos visuales impactantes y realistas.</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="100">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/servicio-al-cliente.png" alt="Image"
                            width="45">
                    </div>
                    <h3 style="color: white;">Servicio<br> Cliente</h3>
                    <p style="color: white;">Nuestro equipo está comprometido en brindarte un servicio excepcional,
                        respondiendo a tus necesidades y garantizando que tu visita sea placentera y sin contratiempos.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4">
                <div class="feature-v1" data-aos="fade-up" data-aos-delay="200">
                    <div class="wrap-icon mb-3">
                        <img src="<?php echo base_url(); ?>/assets/iconos/boleto.png" alt="Image" width="45">
                    </div>
                    <h3 style="color: white;">Boletos<br> Línea</h3>
                    <p style="color: white;">Compra tus boletos de forma rápida y segura desde la comodidad de tu hogar
                        a través de nuestro servicio de venta de boletos en línea, evitando filas y asegurando tu lugar
                        en la película deseada.</p>
                </div>
            </div>

        </div>
    </div>
</div>

</div>
</div>
</div> <!-- END .unslate_co-site-inner -->
