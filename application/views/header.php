<!doctype html>
<html lang="en">

<head>
    <title>CineFlix</title>
    <link rel="icon" href="<?php echo base_url('plantilla/images/cineflixlogo.png'); ?>" type="image/png">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">
    <meta name="keywords" content="html, css, javascript, jquery">
    <meta name="author" content="">
    <!-- Theme Style -->
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/style.css'); ?>">

    <!-- Boton Inicio de Sesión y Registro-->
    <style>
    body {
        background-image: url("<?php echo base_url('assets/banner/nuevo.jpg') ?>");


    }

    .button-container {
        text-align: right;
        margin-top: 5px;
        margin-right: 10px;
        /* Ajusta el margen derecho */
        margin-bottom: 5px;
    }

    .button {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        background-color: #000000;
        border: none;
        color: white;
        text-align: center;
        font-size: 16px;
        padding: 10px;
        width: 160px;
        transition: background-color 0.3s;
        cursor: pointer;
        border-radius: 15px;
        margin-left: 10px;
        text-decoration: none;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);
        /* Agregamos sombra al botón */
    }

    .button:hover {
        background-color: #8B0000;
        /* Rojo oscuro */
        color: #FFFFFF;
        /* Blanco */

    }

    .button-icon {
        margin-right: 5px;
        width: 20px;
        height: 20px;
    }

    .button-text {
        margin-left: 5px;
    }
    </style>


    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vendor/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vendor/animate.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vendor/aos.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vendor/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vendor/jquery.fancybox.min.css'); ?>">
    <!-- importacion pára el corousel -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.theme.default.min.css'); ?>" />
    <!-- Incluir archivo JavaScript de jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <!-- Incluir archivo JavaScript de Owl Carousel -->
    <script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script>

    <style>
    .owl-carousel {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .owl-carousel .item {
        margin: 10px;
        padding: 10px;
        text-align: center;
        background-color: #F6F6F6;
        border-radius: 5px;
    }

    .owl-carousel .item img {
        max-width: 100%;
        height: 800px;
        /* Ajusta el tamaño deseado para todas las imágenes */
        object-fit: cover;
        /* Ajusta el modo de ajuste de la imagen */
        border-radius: 5px;
    }
    </style>

    <style>
    .preloader {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #ffffff;
        z-index: 9999;
        display: flex;
        justify-content: center;
        align-items: center;
        transition: opacity 0.5s ease;
    }

    .preloader.hide {
        opacity: 0;
        pointer-events: none;
    }

    /* Estilos para la imagen */
    .preloader img {
        width: 100px;
        /* Ajusta el ancho de la imagen según tus necesidades */
    }

    .zoom-image {
        animation: zoom 1s infinite linear;
    }

    @keyframes zoom {
        0% {
            transform: scale(1);
        }

        50% {
            transform: scale(1.5);
        }

        100% {
            transform: scale(1);
        }
    }
    </style>



</head>

<body data-spy="scroll" data-target=".site-nav-target" data-offset="200">


    <div class="preloader">

        <h1 class="heading gsap-reveal-hero"
            style="text-shadow: -1px -1px 0 white, 1px -1px 0 white, -1px 1px 0 white, 1px 1px 0 white; font-family: 'Bangers', cursive; letter-spacing: 8px;">
            <span style="color: #FFA500;">Cine</span>
            <span style="color: #00BFFF;">Flix</span>

            <img src="<?php echo base_url('assets/palomitas.gif') ?>" alt="Preloader" class="zoom-image">
        </h1>
    </div>
    <div class="button-container">

        <a href="<?php echo site_url('Auth/login');?>" class="button">
            <span  class="button-text">Iniciar Sesión</span>
            <span class="button-icon"><img src="<?php echo base_url('plantilla/images/male.png'); ?>" width="20px"
                    height="20px"></span>
        <!-- <a href="<?php echo site_url('auth/register');?>" class="button">
            <span class="button-text">Registrarse</span>
            <span class="button-icon"><img src="<?php echo base_url('plantilla/images/key.png'); ?>" width="20px"
                    height="20px"></span>
        </a> -->
    </div>


    <nav class="unslate_co--site-mobile-menu">
        <div class="close-wrap d-flex">
            <a href="#" class="d-flex ml-auto js-menu-toggle">
                <span class="close-label">Salir</span>
                <div class="close-times">
                    <span class="bar1"></span>
                    <span class="bar2"></span>
                </div>
            </a>
        </div>
        <div class="site-mobile-inner"></div>
    </nav>


    <div class="unslate_co--site-wrap">

        <div class="unslate_co--site-inner">

            <div class="lines-wrap">
                <div class="lines-inner">
                    <div class="lines"></div>
                </div>
            </div>
            <!-- END lines -->

      <!-- END nav -->


            <nav class="unslate_co--site-nav site-nav-target">

                <div class="container">

                    <div class="row align-items-center justify-content-between text-left">
                        <div class="col-md-5 text-right">
                            <ul class="site-nav-ul js-clone-nav text-left d-none d-lg-inline-block">
                                <li>
                                    <a href="<?php echo site_url(); ?>" class="nav-link">Incio</a>
                                </li>
                                <li><a href="<?php echo site_url('#portfolio-section'); ?>"
                                        class="nav-link">Promociones</a></li>
                                <li><a href="<?php echo site_url('#about-section'); ?>" class="nav-link">Sobre
                                        Nosotros</a></li>
                                <li><a href="<?php echo site_url('#services-section'); ?>"
                                        class="nav-link">Servicios</a></li>
                            </ul>
                        </div>

                        <div class="site-logo pos-absolute">
                          <a href="<?php echo site_url(); ?>" class="unslate_co--site-logo">CineFlix</a>
                        </div>
                        <div class="col-md-5 text-right text-lg-left">
                            <ul class="site-nav-ul js-clone-nav text-left d-none d-lg-inline-block">

                                <li><a href="<?php echo site_url('compras/nuevo');?>" class="nav-link">Compra-Tickets</a></li>
                                <li><a href="<?php echo site_url('reservas/nuevo');?>"
                                        class="nav-link">Reserva-Tickets</a></li>
                                <li><a href="<?php echo site_url('Auth/login'); ?>" class="nav-link">Cartelera</a></li>
                                <li><a href="<?php echo site_url('ayudas/nuevo');?>" class="nav-link">Ayuda</a></li>

                            </ul>
                            <ul class="site-nav-ul-none-onepage text-right d-inline-block d-lg-none">
                              <li><a href="#" class="js-menu-toggle">Menus</a></li>
                            </ul>

                        </div>

                    </div>

                </div>

            </nav>


            <!-- END nav -->
