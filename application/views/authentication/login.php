<div class="login-box">
	<div class="login-logo">
		<a href="<?php echo base_url(); ?>"> <b style=" color: #40CFFF" >CineFlix</b> </a>
	</div>
	<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg text-bold"> Bienbenido al login de Cineflix</p>
		<form method="post" action="<?php echo base_url('auth/login'); ?>" role="login">
			<div class="form-group has-feedback">
				<input type="email" name="email" class="form-control" required minlength="5" placeholder="Correo electrónico" />
				<span class="glyphicon  glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" name="password" class="form-control" required minlength="5" placeholder="Contraseña" />
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>

			<div class="row">
				<div class="checkbox icheck col-xs-12 col-sm-6 col-md-6">
				  <label>
				    <?php echo form_checkbox('remember_code', '1', false, 'id="remember_code"'); ?>
				    Aceptar Condiciones
				  </label>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6" style="padding-bottom: 5px">
				  <button type="submit" name="submit" value="login" class="btn btn-primary btn-block btn-flat" onclick="return verificarInicioSesion()">
				    <i class="fa fa-sign-in" aria-hidden="true"></i> Ingresar
				  </button>
				</div>
			</div>
			<a href="<?php echo base_url('auth/forgot_password'); ?>"> olvidaste contraseña</a><br>
			<a href="<?php echo base_url('auth/register'); ?>"> registrarte</a>

		</form>

	</div>
	<div id="myalert">
		<?php echo $this->session->flashdata('alert', true); ?>
	</div>
	<br>
	<div class="box box-solid box-info">
		<div class="box-header">
				<h3 class="box-title">Prueba de logeo</h3>
		</div>
		<div class="box-body">
			<b>E-mail</b> : admin@mail.com (administrador) <br>
			<b>E-mail</b> : member@mail.com (miembro)<br>
			<b>Password</b> : password
	</div>
</div>


<script>
	$(function() {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});
	$('#myalert').delay('slow').slideDown('slow').delay(4100).slideUp(600);
</script>
<script>
  function verificarInicioSesion() {
    var checkbox = document.getElementById('remember_code');

    if (checkbox.checked) {
      // La casilla está marcada, se permite el inicio de sesión
      // Agrega aquí tu código para iniciar sesión
      return true; // Permite el envío del formulario
    } else {
      // La casilla no está marcada, muestra un mensaje de error o toma la acción deseada
      alert('Debes aceptar las condiciones antes de iniciar sesión.');
      return false; // Evita el envío del formulario
    }
  }
</script>
