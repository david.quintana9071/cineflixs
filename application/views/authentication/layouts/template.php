<!DOCTYPE html>
<html>

<head>
	<title>
		Login-Cineflix
	</title>
	<link rel="icon" href="<?php echo base_url('plantilla/images/cineflixlogo.png'); ?>" type="image/png">
	<!-- meta -->
	<?php require_once('_meta.php') ;?>

	<!-- css -->
	<?php require_once('_css.php') ;?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<!-- jQuery 2.2.3 -->
	<script src="<?php echo base_url('assets');?>/vendor/jquery/jquery.min.js"></script>

</head>

<body class="hold-transition register-page" style="background-color: yellow;">
	<div class="wrapper">
		<?php echo $contents ;?>
	</div>
	<!-- js -->
	<?php require_once('_js.php') ;?>
</body>

</html>
