<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peliculas extends CI_Controller {
	// definiendo el constructor de la clase
	public function __construct(){
		parent::__construct();
		$this->load->model("Pelicula");
	}
	// renderiza la vista index de jugadores
	public function index(){
		$data["listadoPeliculas"]=$this->Pelicula->obtenerTodos();
		$this->load->view('admin/header');
		$this->load->view('admin/usuario/pelicula/index',$data);
		$this->load->view('admin/footer');
	}
	// renderiza la vista nuevo de jugadores
  public function nuevo(){
    $this->load->view('admin/header');
    $this->load->view('admin/usuario/pelicula/nuevo');
    $this->load->view('admin/footer');
  }
	// funcion para capturar los valores del formulario Nuevo
	public function guardarPelicula(){
		$datosNuevoJugador=array(
      "nombre"=>$this->input->post('nombre'),
			"imagen"=>$this->input->post('imagen')
		);
		// Inicio del proceso de subida de fotografía
		$this->load->library("upload"); //activando la libreria de subida de archivos
	 	$new_name = "foto_" . time() . "_" . rand(1, 5000); //generando nombre aleatorio
	 	$config['file_name'] = $new_name;
	 	$config['upload_path'] = FCPATH . 'uploads/peliculas/'; //ruta de subida
	 	$config['allowed_types'] = 'jpg|png'; //pdf|docx
	 	$config['max_size']  = 5*1024; //5MB
	 	$this->upload->initialize($config); //inicializar la configuración
		//validando la subida de archivo
	 	if ($this->upload->do_upload("imagen")){
		//Que si se subio con éxito
		$dataSubida = $this->upload->data();
		$datosNuevoJugador["imagen"] = $dataSubida['file_name'];
		} //fin del proceso de subida de archivos

		print_r($datosNuevoJugador);
		if ($this->Pelicula->insertar($datosNuevoJugador)) {
				$this->session->set_flashdata('confirmacion','Jugador insertado exitosamente');
		}else{
				$this->session->set_flashdata('error','Error al insertar, verifique e intente de nuevo');
		}
		redirect('member/Peliculas/index');
	}
	//Función para eliminar jugadores
	public function borrar($id){
		if ($this->Pelicula->eliminarPorId($id)){
				$this->session->set_flashdata('confirmacion','Jugador ELIMINADO exitosamente');
		} else {
			  $this->session->set_flashdata('error','Error al eliminar, verifique e intente de nuevo');
		}
		redirect('member/Peliculas/index');
	}
	//Función para renderizar formulario de actualización
	public function actualizar($id){
		$data["peliculaEditar"]=$this->Pelicula->ObtenerPorId($id);
		$this->load->view('admin/header');
		$this->load->view('admin/usuario/pelicula/actualizar',$data);
		$this->load->view('admin/footer');
	}
	//Funcion para procesar botón actualización
	public function procesarActualizacion(){
    $id = $this->input->post("id");
    $datosJugadorEditado = array(
        "nombre" => $this->input->post('nombre')
    );

    // Verificar si se seleccionó una nueva imagen
    if (!empty($_FILES['imagen']['name'])) {
        $this->load->library("upload");
        $new_name = "foto_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name;
        $config['upload_path'] = FCPATH . 'uploads/peliculas/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = 5 * 1024;

        $this->upload->initialize($config);

        if ($this->upload->do_upload("imagen")) {
            $dataSubida = $this->upload->data();
            $datosJugadorEditado["imagen"] = $dataSubida['file_name'];

            // Eliminar la imagen anterior si existe
            $peliculaAnterior = $this->Pelicula->ObtenerPorId($id);
            if ($peliculaAnterior && !empty($peliculaAnterior->imagen)) {
                $rutaImagenAnterior = FCPATH . 'uploads/peliculas/' . $peliculaAnterior->imagen;
                if (file_exists($rutaImagenAnterior)) {
                    unlink($rutaImagenAnterior);
                }
            }
        } else {
            // Error en la carga de la nueva imagen
            $this->session->set_flashdata('error', 'Error al cargar la imagen, verifique e intente de nuevo');
            redirect('member/Peliculas/index/' . $id);
        }
    }

    if ($this->Pelicula->actualizar($id, $datosJugadorEditado)) {
        $this->session->set_flashdata('confirmacion', 'Jugador ACTUALIZADO exitosamente');
    } else {
        $this->session->set_flashdata('error', 'Error al actualizar, verifique e intente de nuevo');
    }

    redirect('member/Peliculas/index');
}

}//cierre de la clase NO BORRAR
