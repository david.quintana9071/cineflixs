<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {

        $this->load->view('member/header');
        $this->load->view('member/welcome_message');
        $this->load->view('member/footer');
    }

}
