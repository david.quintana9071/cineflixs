<?php

class Reservas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Reserva'); // Carga el modelo Ayuda
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('reservas/nuevo');
      $this->load->view('footer');
    }

    public function guardarReserva()
    {
        // Utiliza el modelo Ayuda
        $datosNuevaReserva = array(
            "nombre_res" => $this->input->post('nombre_res'),
            "pelicula_res" => $this->input->post('pelicula_res'),
            "aciento_res" => $this->input->post('aciento_res'),
            "costo_res" => $this->input->post('costo_res'),
            "descripcion_res" => $this->input->post('descripcion_res'),
					 "fecha_res" => $this->input->post('fecha_res')
        );

        print_r($datosNuevaReserva);

        if ($this->Reserva->insertar($datosNuevaReserva)) { // Accede al método insertar del modelo Ayuda
            redirect('reservas/nuevo');
        } else {
            echo "<h1>ERROR</h1>";
        }
    }

    // Resto del código del controlador
}
