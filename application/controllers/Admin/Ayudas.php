<?php
class Ayudas extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Ayuda"); // Cargar el modelo Ayuda
  }

  public function index()
  {
    $data["listadoAyudas"] = $this->Ayuda->obtenerTodos();
    $data["mostrarIdColumn"] = false; // O true, según tus necesidades
    $this->load->view('admin/header');
    $this->load->view('admin/ayudas/index', $data); // Pasar los datos a la vista
    $this->load->view('admin/footer');
  }
  //funcion para dar por atendido ayuda
  public function borrar($id_lbq){
    if ($this->Ayuda->eliminarPorId($id_lbq)) {
      redirect('admin/ayudas/index');
    } else {
      echo "Error al eliminar :(";
    }
  }
}
