<?php
class Compras extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Compra"); // Cargar el modelo Ayuda
  }

  public function index()
  {
    $data["listadoCompras"] = $this->Compra->obtenerTodos();
    $data["mostrarIdColumn"] = false; // O true, según tus necesidades
    $this->load->view('admin/header');
    $this->load->view('admin/compras/index', $data); // Pasar los datos a la vista
    $this->load->view('admin/footer');
  }
  //funcion para dar por atendido ayuda
  public function borrar($id_com){
    if ($this->Compra->eliminarPorId($id_com)) {
      redirect('admin/compras/index');
    } else {
      echo "Error al eliminar :(";
    }
  }
}
