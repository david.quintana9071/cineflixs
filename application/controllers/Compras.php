<?php

class Compras extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Compra'); // Carga el modelo Ayuda
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('compras/nuevo');
      $this->load->view('footer');
    }

    public function guardarCompra()
    {
        // Utiliza el modelo Ayuda
        $datosNuevaReserva = array(
            "cedula_com" => $this->input->post('cedula_com'),
            "primer_apellido_com" => $this->input->post('primer_apellido_com'),
            "segundo_apellido_com" => $this->input->post('segundo_apellido_com'),
            "nombre_com" => $this->input->post('nombre_com'),
            "funcion_com" => $this->input->post('funcion_com'),
					 "telefono_com" => $this->input->post('telefono_com'),
           "direccion_com" => $this->input->post('direccion_com'),
           "asientos_com" => $this->input->post('asientos_com'),
           "precio_com" => $this->input->post('precio_com')
        );

        print_r($datosNuevaReserva);

        if ($this->Compra->insertar($datosNuevaReserva)) { // Accede al método insertar del modelo Ayuda
            redirect('compras/nuevo');
        } else {
            echo "<h1>ERROR</h1>";
        }
    }

    // Resto del código del controlador
}
