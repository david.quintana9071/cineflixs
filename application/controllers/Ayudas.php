<?php

class Ayudas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Ayuda'); // Carga el modelo Ayuda
    }
    public function nuevo(){
      $this->load->view('header');
      $this->load->view('ayudas/nuevo');
      $this->load->view('footer');
    }

    public function guardarAyuda()
    {
        // Utiliza el modelo Ayuda
        $datosNuevaAyuda = array(
            "correo_lbq" => $this->input->post('correo_lbq'),
            "usuario_lbq" => $this->input->post('usuario_lbq'),
            "motivo_lbq" => $this->input->post('motivo_lbq'),
            "asunto_lbq" => $this->input->post('asunto_lbq'),
            "descripcion_lbq" => $this->input->post('descripcion_lbq')
        );

        print_r($datosNuevaAyuda);

        if ($this->Ayuda->insertar($datosNuevaAyuda)) { // Accede al método insertar del modelo Ayuda
            redirect('ayudas/nuevo');
        } else {
            echo "<h1>ERROR</h1>";
        }
    }

    // Resto del código del controlador
}
