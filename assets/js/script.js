document.getElementById("loginButton").addEventListener("click", function() {
  document.getElementById("loginPopup").style.display = "block";
});

// Cerrar la ventana emergente al hacer clic fuera de ella
window.addEventListener("click", function(event) {
  var loginPopup = document.getElementById("loginPopup");
  if (event.target == loginPopup) {
    loginPopup.style.display = "none";
  }
});
